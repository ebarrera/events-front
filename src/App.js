import React, { Component } from 'react';

import Layout from './hoc/Layout/Layout';
import Workspace from './containers/Workspace/Workspace';

class App extends Component {
  render () {
    return (
      <div>
        <Layout>
          <Workspace />
        </Layout>
      </div>
    );
  }
}

export default App;
