import axios from 'axios';

class ClientEventCore {

    constructor(){
        this.instance = axios.create({
            baseURL: 'http://localhost:9878/ws',
            timeout: 5000,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    getEvents = () => {
        return this.instance.get( '/events' );
    }

    create = (evt) => {
        return this.instance.post( '/events', evt );
    }

    delete = (uuid) => {
        return this.instance.delete( '/events/' + uuid );
    }

    update = (evt) => {
        return this.instance.put( '/events/' +  evt.eventId, evt );
    }

}

export default ClientEventCore;