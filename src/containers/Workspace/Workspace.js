import React, { Component } from 'react';

import classes from './Workspace.css';
import ListCards from '../ListCards/ListCards';
import Button from '../../components/UI/Button/Button';
import Modal from '../../components/UI/Modal/Modal';
import Spinner from '../../components/UI/Spinner/Spinner';
import Aux from '../../hoc/Aux/Aux';

import ClientEventCore from '../../utils/clients/ClientEventCore';

let eventCoreInstance = null;

class Workspace extends Component {
    state = {
        events: null,
        error: false,
        errorMessage: null,
        loading: false,
        addingEvent: false,
        updatingMode: false,
        evt: {
            title: '',
            description: '',
            atDate: ''
        }
    }

    constructor(props) {
        super(props);
        eventCoreInstance = new ClientEventCore();
    }

    componentDidMount() {
        eventCoreInstance.getEvents()
            .then(response => {
                this.setState({ events: response.data })
            })
            .catch(error => {
                this.setState({ error: true });
            });
    }

    resetEvent = () => {
        let newState = { ...this.state };
        newState.evt = {
            title: '',
            description: '',
            atDate: ''
        }

        this.setState(newState);
    }

    addEventHandler = () => {
        this.setState({ addingEvent: true });
    }

    cancelAddEventHandler = () => {
        this.resetEvent();
        this.setState({ addingEvent: false, updatingMode: false });
    }

    changeHandler = (e) => {
        let newState = Object.assign({}, this.state);
        newState.evt[e.target.name] = e.target.value;
        this.setState(newState);
    }

    createEventHandler = () => {
        this.setState({ loading: true });

        eventCoreInstance.create(this.state.evt)
            .then(response => {
                this.resetEvent();
                this.setState({ loading: false, addingEvent: false });
                this.componentDidMount();
            })
            .catch(error => {
                this.setState({ loading: false, addingEvent: false });
            });
    }

    deleteEventHandler = ( uuid ) => {
        this.setState({ loading: true });

        eventCoreInstance.delete( uuid )
            .then( response => {
                this.resetEvent();
                this.setState({ loading: false, addingEvent: false });
                this.componentDidMount();
            })
            .catch(error => {
                this.setState({ loading: false, addingEvent: false });
            });
    }

    updateFireEventHandler = (evt) => {
        console.log(evt);
        this.setState( {evt: evt, addingEvent: true, updatingMode: true} );
    }

    updateEventHandler = () => {
        this.setState({ loading: true });

        eventCoreInstance.update(this.state.evt)
            .then(response => {
                this.resetEvent();
                this.setState({ loading: false, addingEvent: false });
                this.componentDidMount();
            })
            .catch(error => {
                this.setState({ loading: false, addingEvent: false });
            });
    }

    render() {
        let cards = this.state.error ? <p>Events can't be loaded</p> : <Spinner />;
        let button = null;

        if (this.state.events) {
            cards = <ListCards events={this.state.events} delete={this.deleteEventHandler} update={this.updateFireEventHandler}/>;
            button = <Button btnType="Success" clicked={this.addEventHandler}>Add event</Button>;
        }

        let btnSuccesModal = <Button btnType="Success" clicked={this.createEventHandler}>Create</Button>;

        if(this.state.updatingMode){
            btnSuccesModal = <Button btnType="Success" clicked={this.updateEventHandler}>Update</Button>
        }

        let form = (
            <Aux>
                <div className={classes.Form_group}>
                    <input type="text" required="required" name="title" value={this.state.evt.title} onChange={(e) => this.changeHandler(e)} />
                    <label htmlFor="input" className={classes.Control_label}>Title</label>
                    <i className={classes.Bar}></i>
                </div>
                <div className={classes.Form_group}>
                    <textarea required="required" name="description" value={this.state.evt.description} onChange={(e) => this.changeHandler(e)}></textarea>
                    <label htmlFor="textarea" className={classes.Control_label}>Description</label>
                    <i className={classes.Bar}></i>
                </div>
                <div className={classes.Form_group}>
                    <input id="date"
                        type="datetime-local" required="required"
                        min="2018-08-01T00:00" max="2020-12-31T00:00"
                        name="atDate" value={this.state.evt.atDate} onChange={(e) => this.changeHandler(e)} />
                    <label htmlFor="date" className={classes.Control_label}>Date</label>
                    <i className={classes.Bar}></i>
                </div>
                {btnSuccesModal}
                <Button btnType="Danger" clicked={this.cancelAddEventHandler}>Cancel</Button>
            </Aux>
        );

        if (this.state.loading) {
            form = <Spinner />
        }

        return (
            <Aux>
                <Modal show={this.state.addingEvent} modalClosed={this.cancelAddEventHandler}>
                    {form}
                </Modal>
                <div className={classes.Workspace}>
                    {cards}
                    {button}
                </div>
            </Aux>
        );
    }

}

export default Workspace;