import React, {Component} from 'react';

import classes from './ListCards.css';
import Card from '../../components/Card/Card';
import Aux from '../../hoc/Aux/Aux';

class ListCards extends Component {

    executeDeleteFunctionHandler = ( deleteFn, uuid ) => {
        deleteFn(uuid);
    }

    exectuteUpdateFunctionHandler = ( updateFn, evt ) => {
        updateFn(evt);
    }

    render(){
        let cards = null;

        if( this.props.events ){
            cards = this.props.events.map( event => {
                return <Card 
                            key={event.eventId}
                            uuid={event.eventId}
                            title={event.title}
                            description={event.description}
                            onDelete={() => this.executeDeleteFunctionHandler(this.props.delete, event.eventId)}
                            onUpdate={() => this.exectuteUpdateFunctionHandler(this.props.update, event)}
                            atDate={event.atDate}/>;
            });
        }

        return (
            <Aux>
                <h3>My events</h3>
                <div className={classes.ListCards}>
                    {cards}  
                </div>
            </Aux>
        );
    }

}

export default ListCards;