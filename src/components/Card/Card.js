import React, {Component} from 'react';

import classes from './Card.css';
import editButton from '../../assets/images/edit.png';
import deleteButton from '../../assets/images/delete.png';

class Card extends Component {

    render() {
        
        return (
            <div className={classes.Card}> 
                <input id={this.props.uuid} type="checkbox" name="tabs" className={classes.Card_input}/>
                <label htmlFor={this.props.uuid} className={classes.Card_title}>{this.props.title}</label>
                <div className={classes.Card_content}>
                    <p>{this.props.description}</p>
                    <div className={classes.Controls}>
                        <div>{this.props.atDate}</div>
                        <div>
                            <img onClick={this.props.onUpdate} src={editButton} alt={"Edit" + this.props.title}/>
                            <img onClick={this.props.onDelete} src={deleteButton} alt={"Delete " + this.props.title}/>
                        </div>                        
                    </div>
                </div>
            </div>
        );
    }

}

export default Card;